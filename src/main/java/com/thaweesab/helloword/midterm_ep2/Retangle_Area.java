/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.midterm_ep2;

/**
 *
 * @author acer
 */
public class Retangle_Area extends Mix_Area {
    
   public  Retangle_Area(double width,double height){
       super(width,height);
   }
   
   @Override
   public double Area(){
       return width * height ;
   }
  
}
