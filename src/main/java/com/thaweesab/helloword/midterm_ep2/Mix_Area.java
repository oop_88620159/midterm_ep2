/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.midterm_ep2;

/**
 *
 * @author acer
 */
public class Mix_Area {
    protected double width;
    protected  double height;
    
   public Mix_Area(double width,double height){
       this.width = width;
       this.height = height;
   } 
   
   public double Area(){
       return width * height;
   }
   
}
