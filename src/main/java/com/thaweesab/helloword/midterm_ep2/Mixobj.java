/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.midterm_ep2;

/**
 *
 * @author acer
 */
public class Mixobj {
    private String name;
    private String spaceA; // space shape
    private String service; // Srevice that the child chooses
    private double price; 
    private double size;
    private double width;
    private double height;
    
    
   public Mixobj(String service,String name, String spaceA ,double width,double height, double price){
       this.service = service;
       this.name = name;
       this.spaceA = spaceA ;
       this.price = price;
       this.width = width;
       this.height = height;
       
       if(spaceA.equals("retangle")){
           Retangle_Area retangle = new Retangle_Area (width,height);
           size = retangle.Area();
       } else if(spaceA.equals("square")){
           Square_Area square = new Square_Area (width,height);
           size = square.Area();
       } else if(spaceA.equals("triangle")){
           Triangle_Area triangle = new Triangle_Area (width,height);
           size = triangle.Area();
       } else if(spaceA.equals("circle")){
           Circle_Area circle = new Circle_Area (width,height);
           size = circle.Area();
       } 
       
       
   }
   
   public double Free(){
       return size * price;
   }
   public String toString(){
       return "Service:"+service+" Mr:"+name+" size:"+this.getS()+" price:"
               + this.getP() +" Bath"+ " Total:"+this.Free()+" Bath" ;
   }
   public double getS(){
       return size;
   } 
   public double getP(){
       return price;
   }
   public boolean set(char p ,double num){ //Overlaod
       if( p =='p'){
           price = num ;
       } else if ( p =='s'){
           size = num ;
       }
       return false;
   }
   public boolean set(char n,String names){  //Overlaod
       if( n =='n'){
           name = names;
       } else if ( n =='s'){
          service = names ;
       }
       return false;
   }
   
}
